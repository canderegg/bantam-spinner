# bantam-spinner

## Purpose

The `bantam-spinner` module implements a simple set of CSS spinner widgets for BantamJS. Each widget uses a single div element with the relevant classes applied, that is then styled into a loading spinner.

## Installation

The CSS can be downloaded [here](files/1.0/bantam-spinner-1.0.min.css) and linked directly into your app. Alternatively, `bantam-spinner` is a module available through the [Bantam Coop](https://canderegg.gitlab.io/bantam-coop/) module manager.

To install the module through Bantam Coop, use the following command:

```bash
$ python coop.py add bantam-spinner +1.0
```

## Usage

The three types of spinners implemented in the module, from left to right: wheel, dots, and streak.

![Spinner animations](spinners.gif)

Each spinner is represented with a single `div` element, with the `bt-spinner` class, as well as one of the following spinner types: `spinner-wheel`, `spinner-dots`, or `spinner-streak`. This div will be styled into a loading spinner widget of the specified type.

The color of the wheel- and streak-type spinners is customizable by setting the `border-color` style attribute on the element. The color of the dots-type spinner is customizable by setting the `color` style attribute on the element. 

A code example with each spinner might be implemented as follows:

```html
<div class="bt-spinner spinner-wheel" style="border-color: #F00;">Loading...</div>

<div class="bt-spinner spinner-dots" style="color: #0F0;">Loading...</div>

<div class="bt-spinner spinner-streak" style="border-color: #00F;">Loading...</div>
```

## License

This module is distributed under the [MIT License](LICENSE).
